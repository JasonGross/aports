# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=dua
pkgver=2.23.0
pkgrel=0
pkgdesc="Tool to conveniently learn about the usage of disk space"
url="https://github.com/Byron/dua-cli"
license="MIT"
arch="all"
makedepends="cargo cargo-auditable"
source="https://github.com/Byron/dua-cli/archive/v$pkgver/dua-$pkgver.tar.gz
	fix-32bit-test.patch
	"
builddir="$srcdir/dua-cli-$pkgver"
options="net" # fetch dependencies

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/$pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
ed258aba3fd355efd4232eccc6ddd7aa77c6855498b2fff572916e3d8b6bc8b605a0cd57fe1fc6cae1ed16e4322ecba2daeca4a49d2677eeb727d83a513c2d95  dua-2.23.0.tar.gz
4369c659172d56236e39f6f408de70cd4585e14214350110a4ada4a097a7bfc6d59aa6bfbfa3872366bdbc6c208941bf36026cf5542aa04ba7a8346343a71621  fix-32bit-test.patch
"
