# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=dolt
pkgver=1.29.7
pkgrel=0
pkgdesc="Dolt – It's Git for Data"
url="https://www.dolthub.com"
arch="all !x86 !armhf !armv7" # fails on 32-bit
license="Apache-2.0"
options="!check chmod-clean net"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/dolthub/dolt/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgver/go"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	mkdir -p build
	go build \
		-mod=readonly \
		-ldflags "-extldflags \"$LDFLAGS\"" \
		-o build \
		./cmd/...
}

package() {
	install -Dm755 build/dolt "$pkgdir"/usr/bin/dolt
}

sha512sums="
bbc1123032b68e946522b918036cb67ae2fcb183b4f8ca3a9fcfd125c10730a69dfd0869589a15d508f3a89101ecb9f883dc69e2412820cf6219ad2cacc1a8bb  dolt-1.29.7.tar.gz
"
